<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Storage;
use File;
use Cookie;
use App\Task;
use App\User;

class TaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show task list page.
     *
     * @return \Illuminate\Http\Response
     */
    public function showList(Request $request)
    {
        $data = $request->validate([
            'search' => 'min:0|max:255|nullable|regex:/(^[A-Za-z0-9 ]+$)+/',
        ]);

        $hide_finished_tasks = $request->cookie('hide_finished_tasks');

        if ($hide_finished_tasks === 'true') {
            $tasks = Task::where('finished', 0);
        } else {
            $tasks = Task::orderBy('finished', 'asc');
        }

        if ($request->has('search')) {
            $tasks
                ->where('title', 'LIKE', '%' . $request->search . '%')
                ->orWhere('description', 'LIKE', '%' . $request->search . '%');
        }

        return view('pages.task.list', [
            'tasks' => $tasks->orderBy('updated_at', 'desc')->paginate(5), 
            'search' => $request->has('search') ? $request->search : '', 
            'hide_finished_tasks' => $hide_finished_tasks
        ]);
    }

    /**
     * Show the task create page.
     *
     * @return \Illuminate\Http\Response
     */
    public function showCreate()
    {
        return view('pages.task.create', [
            'users' => User::all()
        ]);
    }

    /**
     * Store a new task.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => 'required|max:255',
            'description' => 'min:0',
            'attachment' => 'file|mimes:pdf,xml,jpg,jpeg|max:5000',
            'user' => 'integer|nullable|min:1'
        ]);

        $file = $request->file('attachment');
        $path = storage_path('app/public/tasks/attachments/');

        if(!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }
        
        if ($file){
            $request->file('attachment')->move($path, $file->getClientOriginalName());
        }

        $task = Task::create([
            'title'                 => $data['title'],
            'description'           => (isset($data['description'])) ? $data['description'] : null,
            'attachment'            => ($file) ? $file->getClientOriginalName() : null,
            'id_user'               => (isset($data['user'])) ? $data['user'] : null,
        ]);

        return redirect(route('task.show.update',[
            'task' => $task->id_task
        ]));
    }    

    /**
     * Show the task edit page.
     *
     * @return \Illuminate\Http\Response
     */
    public function showEdit(Task $task)
    {
        return view('pages.task.edit', [
            'users' => User::all(),
            'task' => $task
        ]);
    } 

    /**
     * Update a task.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        $data = $request->validate([
            'title' => 'required|max:255',
            'description' => 'min:0',
            'attachment' => 'file|mimes:pdf,xml,jpg,jpeg|max:5000',
            'user' => 'nullable|integer|min:1',
            'finished' => 'required|boolean',
        ]);

        $file = $request->file('attachment');
        if ($file){
            $file->move('tasks/attachments/',$file->getClientOriginalName());
        }

        $task->title = $data['title'];
        $task->description = $data['description'];
        $task->attachment = ($file) ? $file->getClientOriginalName() : $task->attachment;
        $task->id_user = ($data['user'] !== '') ? $data['user'] : null;
        $task->finished = $data['finished'];
        $task->update();

        return redirect(route('task.show.update', [
            'task' => $task->id_task
        ]));
    }

    /**
     * Delete a task.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Task $task)
    {        
        $task->delete();

        return redirect(route('task.list'));
    }             
}
