<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $primaryKey = 'id_task';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'finished', 'attachment', 'id_user'
    ];

    public function user() 
    {
        return $this->belongsTo(User::class, 'id_user');
    }
}
