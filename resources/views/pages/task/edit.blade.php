@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Task</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('task.update',['task' => $task->id_task]) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-4 control-label">Title*</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control" placeholder="Title" name="title" value="{{ old('title') ? old('title') : $task->title }}" autofocus>

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Description</label>

                            <div class="col-md-6">
                                <textarea id="description" class="form-control" placeholder="Description" name="description">{{ old('description') ? old('description') : $task->description }}</textarea>

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>                        

                        <div class="form-group{{ $errors->has('attachment') ? ' has-error' : '' }}">
                            <label for="attachment" class="col-md-4 control-label">Attachment</label>

                            <div class="col-md-6">
                                <a href="{{ asset('storage/tasks/attachments/'.$task->attachment) }}" target="blank">{{ $task->attachment }}</a>
                                <input type="file" id="attachment"  name="attachment" accept="*/*">

                                @if ($errors->has('attachment'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('attachment') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('user') ? ' has-error' : '' }}">
                            <label for="user" class="col-md-4 control-label">Assignee</label>

                            <div class="col-md-6">
                                <select name="user" class="form-control">                                    
                                    <option value="">Please select a user</option>
                                    @foreach($users as $user)
                                        <option 
                                            value="{{ $user->id }}" 
                                            @if(old('user') && old('user') === (string)$user->id || $task->id_user === $user->id)
                                                selected
                                            @endif
                                        >
                                            {{ $user->name }}
                                        </option>
                                    @endforeach
                                </select>

                                @if ($errors->has('user'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('finished') ? ' has-error' : '' }}">
                            <label for="finished" class="col-md-4 control-label">Status</label>

                            <div class="col-md-6">
                                <select name="finished" class="form-control">                                    
                                    <option value="0" @if(old('finished') === '0' || $task->finished === 0) selected @endif>In progress</option>
                                    <option value="1" @if(old('finished') === '1' || $task->finished === 1) selected @endif>Finished</option>
                                </select>

                                @if ($errors->has('finished'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('finished') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <a href="{{ route('task.list') }}" class="btn btn-default">Cancel</a>
                                <a href="{{ route('task.delete',['task' => $task->id_task]) }}" class="btn btn-danger">Delete</a>
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection