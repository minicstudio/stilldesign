@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Tasks</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('task.list') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('search') ? ' has-error' : '' }}">
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="search" value="@if(old('search')){{ old('search') }}@else{{ $search }}@endif" placeholder="Search"/>
                                @if ($errors->has('search'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('search') }}</strong>
                                    </span>
                                @endif                                
                            </div>
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-primary pull-right">
                                    Search
                                </button>
                            </div>       
                        </div>
                    </form>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="pull-left">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="hide-finished-tasks" @if($hide_finished_tasks === 'true') checked="" @endif name="hide_finished_tasks" onchange="hideFinishedTasks(this)">Hide finished tasks
                                    </label>
                                </div>
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('task.show.create') }}" class="btn btn-primary pull-right">+ Create a new Task</a>
                            </div>
                        </div>
                    </div>                    
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Created</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tasks as $task)
                        <tr>
                            <th scope="row">{{ $task->id_task }}</th>
                            <td>{{ $task->title }}</th>
                            <td>{{ $task->description }}</th>
                            <td>{{ $task->created_at->diffForHumans(null,false,true,2) }}</th>
                            <td>{{ $task->finished ? 'Finished' : 'In progress' }}</th>
                            <td><a href="{{ route('task.show.update',['task' => $task->id_task]) }}">View</a></th>
                        </tr>
                        @endforeach                                                                                               
                    </tbody>
                </table>
                <div class="col-md-12">
                    <div class="pull-right">
                        {{ $tasks->links() }}
                    </div>
                </div>                
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function hideFinishedTasks(e) {
        var d = new Date();
        d.setTime(d.getTime() + (30*24*60*60*1000));        
        document.cookie = "hide_finished_tasks=" + e.checked + "; expires=" + d.toUTCString() + "; path=/";
        window.location.href = "/tasks/list";
    }
</script>
@endsection