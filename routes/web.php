<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::group(['prefix' => 'tasks', 'as' => 'task.', 'middleware' => ['auth']], function(){
    Route::any('/list', [
        'as'=>'list',
        'uses'=>'TaskController@showList'
    ]);
    Route::get('create', [
        'as'=>'show.create',
        'uses'=>'TaskController@showCreate'
    ]);
    Route::post('create', [
        'as'=>'store',
        'uses'=>'TaskController@store'
    ]);
    Route::get('{task}', [
        'as'=>'show.update',
        'uses'=>'TaskController@showEdit'
    ]);
    Route::get('{task}/delete', [
        'as'=>'delete',
        'uses'=>'TaskController@delete'
    ]);    
    Route::post('{task}', [
        'as'=>'update',
        'uses'=>'TaskController@update'
    ]);    
});
