<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Task;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Teszt Elek',
            'email' => 'teszt.elek@test.com',
            'password' => bcrypt('test'),
        ]);

        Task::create([
        	'title' => 'Ez egy uj task',
        	'description' => 'Ez az uj task leirasa',
        	'finished' => 0,
        	'id_user' => $user->id
        ]);

        Task::create([
        	'title' => 'Ez egy teljesen mas task',
        	'description' => 'Ez a teljesen mas task leirasa',
        	'finished' => 1,
        	'id_user' => $user->id
        ]);        
    }
}
